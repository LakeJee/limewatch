# LIME WATCH

**NOTE: This current version only runs on Windows.**

This is inteded to help my workflow when programming in C or C++

### BUILDING
Run the `build.bat` script in the project root directory. This will generate a `watcher.exe` file that can be copy/pasted into any directory that you want to watch.

By default it will watch the entire sub directory tree that you that drop it into.

### TODO
- [x] Allow arguments to be passed in. (Watch sub directories, exclude file types, etc.)
- [X] Search for `build.sh` or `build.bat` file instead of manually adding flags. (Ease of use)
- [X] Run build script once change is detected.
- [ ] Update to use ignore_files.txt to tell Watcher to ignore certain file types.
- [ ] Platform independent file watching 

### BUGS
|RESOLVED?|RESOLVED DATE|DISCOVERY DATE|BUG|
|---|---|---|---|
|:heavy_check_mark:|09/10/2021|09/01/2021|Watcher will produce two events per notification. Known issue when monitoring OS system activities and events|
|:heavy_check_mark:|09/13/2021|09/13/2021|Watcher, *AGAIN* will produce two events per notification. Known issue when monitoring OS system activities and events.|
|:heavy_check_mark:|11/1/2021|09/27/2021|Reported memory leaks when allocating memory to `path` variable and `buffer` for storing file paths. But when trying to free the memory, the program crashes...|
|||2/28/2022|Watcher fails to run in some terminal emulators on Windows (Hyper, Tabby, etc). So far it only works in the basic CMD Windows shell, Windows Powershell, and Windows Terminal running CMD.exe| 