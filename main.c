#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <fileapi.h>
#include <stdlib.h>
#include <assert.h>
#include <stdarg.h>
#include "dirent/include/dirent.h"

/*constants*/
#define MAX_DIRS               1000
#define MAX_FILES              2000
#define MAX_BUFFER             4096
#define USAGE_BAR              "*******************\n"
#define USAGE_TITLE            "*      USAGE      *\n"
#define BUILD_BAT              "build.bat"
#define BUILD_SH               "build.sh"
#define WIN_FILE_SEPARATOR     "\\"
#define WIN_FILE_SEPARATOR_LEN (sizeof(WIN_FILE_SEPARATOR) - 1)
#define GIT_FOLDER             ".git"
#define GIT_IGNORE             ".gitignore"
#define LICENSE                "LICENSE"
#define VSCODE                 ".vscode"
#define GITMODULES             ".gitmodules"
#define DAT                    "dat"
#define CODER                  "project.4coder"
#define CHANGELOG              "ChangeLog"
#define DOT                    "."
#define PREV_DIR               ".."
#define EXE                    "exe"
#define OBJ                    "obj"
#define TXT                    "txt"
#define MD                     "md"
#define JSON                   "json"

/* global variables */
// NOTE (Lake): These can probably become local variables at some point.
int WATCH_SUB_DIRS = 0;
int BUILD_SCRIPT_FOUND = 0;
int USE_BAT = 0;
int USE_SH = 0;

/* functions */
int  parse_args(char *arg1, char *arg2, char *arg3);
int  check_for_build(char *check);
int  check_current_dir(char *dirToWatch, int fromDir, int *level, char **buildPath, char *fullFileList[], int *totalFiles);
int  skip_standard_dirs_files(char *directoryOrFileName);
int  match_on_file_extension(char *fileName, char *file_extension, int extensionLength, size_t fileNameLength);
long init_file_read(char *fileName);
char *get_just_file_name(char *fullFileName);
void append(char *s, char c);
void get_file_size(char *fileName, long *fileSize, long *initFileSize, int firstPass, char **buildPath, char *cdToBuildPath);
void show_found_files(char *files[]);
void file_check(int *current_file, char *buffer[], char *fileName, char *dirToWatch);
void get_length_of_extension_name(char *fileName, int *extensionLength, int *charsToDot, size_t *fileNameLength);
void print_tab_per_level(int level);
void use_bat_or_shell();
void print_usage();

/* implementations */
int parse_args(char *arg1, char *arg2, char *arg3)
{
	printf("\n--Selected Configuration--\n\n");
	if (strcmp(arg1, "true") == 0)
	{
		printf("Watch sub directories: true\n");
		WATCH_SUB_DIRS = 1;
	} else if (strcmp(arg1, "false") == 0) {
		printf("Watch sub directories: false\n");
		WATCH_SUB_DIRS = 0;
	} else {
		printf("Unrecognized argument.\n");
		return 0;
	}
	return 1;
}

int check_for_build(char *check)
{
	if (strcmp(check, BUILD_BAT) == 0)
	{
		USE_BAT = 1;
		return 1;
	} else if (strcmp(check, BUILD_SH) == 0)
	{
		USE_SH = 1;
		return 1;
	} else {
		return 0;
	}
}

int check_current_dir(char *dirToWatch, 
                      int fromDir, 
                      int *level, 
                      char **buildPath, 
                      char *fullFileList[], 
                      int *totalFiles)
{
	// Pointer to DIR struct.
	// If the opendir function call returns successfully then we're in business baby
	DIR *d = opendir(dirToWatch);
	if (d)
	{
		struct dirent *dir;
		struct stat st;
		
		(*totalFiles) = 0;
		
		while ((dir = readdir(d)) != NULL)
		{
			int fileExtensionNameLength = 0;
			int charsToDot = 0; 
			size_t fileNameLength = 0;
			char *path = NULL;
			char *extension = NULL;
			
			
			
			path = malloc(sizeof(char) * MAX_DIRS + 1);
			strcpy(path, dirToWatch);
			strcat(path, WIN_FILE_SEPARATOR);
			
			// currently, we ignore the most common dot files and the generated binary becuase wildcards are hard...
			// update 1: fuck a glob we are getting the extension on our own homie!!
			fileNameLength = strlen(dir->d_name);
			fileExtensionNameLength = 0;
			charsToDot = 1;
			
			get_length_of_extension_name(dir->d_name, &fileExtensionNameLength, &charsToDot, &fileNameLength);
			extension = malloc(sizeof(char) * fileExtensionNameLength + 1);
			
			
			if (skip_standard_dirs_files(dir->d_name))
			{
				continue;
			} else {
				if (dir->d_type != DT_DIR)
				{
					if (match_on_file_extension(dir->d_name, extension, fileExtensionNameLength, fileNameLength))
					{
						free(extension);
						continue;
					} else {
						free(extension);
					}
				}
			}
			
			switch(dir->d_type)
			{
				case DT_UNKNOWN: {
					printf("Unknown file type. Ignoring.");
					break;
				}
				case DT_DIR: {
					if (fromDir)
					{
						print_tab_per_level(*level);
						printf("Directory -- name: %s\n", dir->d_name);
						print_tab_per_level(*level);
						printf("Getting directory contents...\n");
						(*level)++;
						
					} else {
						(*level) = 0;
						printf("Directory -- name: %s\n", dir->d_name);
						printf("Getting directory contents...\n");
						(*level)++;
					}
					
					strcpy(path, dirToWatch);
					strcat(path, WIN_FILE_SEPARATOR);
					strcat(path, dir->d_name);
					
					check_current_dir(path, 1, level, buildPath, fullFileList, totalFiles);
					
					break;
				}
				case DT_REG: {
					if (fromDir)
					{
						print_tab_per_level(*level);
						printf("Regular file -- full path name: %s%s\n", path, dir->d_name);
						
						BUILD_SCRIPT_FOUND = check_for_build(dir->d_name);
						if(BUILD_SCRIPT_FOUND)
						{
							*buildPath = malloc(sizeof(char) * (strlen(path) + strlen(dir->d_name) + 1));
							strcpy(*buildPath, path);
						}
					} else {
						printf("Regular file -- full path name: %s%s\n", path, dir->d_name);
						BUILD_SCRIPT_FOUND = check_for_build(dir->d_name);
						if(BUILD_SCRIPT_FOUND)
						{
							*buildPath = malloc(sizeof(char) * (strlen(path) + strlen(dir->d_name) + 1));
							strcpy(*buildPath, path);
						}
					}
					
					file_check(totalFiles, fullFileList, dir->d_name, dirToWatch);
					break;
				}
			}
			
			// free the memory allocated to path for the next run
			free(path);
		}
	}
	closedir(d);
	return 1;
}

int skip_standard_dirs_files(char *directoryOrFileName)
{
	if (strcmp(directoryOrFileName, DOT)        == 0 || 
			strcmp(directoryOrFileName, PREV_DIR)   == 0 || 
			strcmp(directoryOrFileName, GIT_FOLDER) == 0 ||
			strcmp(directoryOrFileName, GIT_IGNORE) == 0 ||
			strcmp(directoryOrFileName, CHANGELOG)  == 0 ||
			strcmp(directoryOrFileName, CODER)      == 0 ||
			strcmp(directoryOrFileName, LICENSE)    == 0 ||
			strcmp(directoryOrFileName, GITMODULES) == 0 ||
			strcmp(directoryOrFileName, "file")     == 0)
	{
		printf("IGNORING -- %s\n", directoryOrFileName);
		return 1;
	} else {
		printf("KEEPING -- %s\n", directoryOrFileName);
		return 0;
	}
}

int match_on_file_extension(char *fileName, 
                            char *file_extension, 
                            int extensionLength, 
                            size_t fileNameLength)
{
	if (strchr(fileName, '.') != NULL)
	{
		for (int i = 0; i < extensionLength; i++)
		{
			file_extension[i] = fileName[fileNameLength - (extensionLength - i)];
		}
		file_extension[extensionLength] = '\0';
		
		/*
		 * Here we read the file extension and if it is not a file type we are currently watching,
		 * we skip the file completely and continue indexing...
		 * update 1: We should probably feed file_extension into a loop and match on an array of extensions read from a file
		 */
		if (strcmp(file_extension, EXE) == 0 ||
				strcmp(file_extension, OBJ) == 0 ||
				strcmp(file_extension, TXT) == 0 ||
				strcmp(file_extension, DAT) == 0 ||
				strcmp(file_extension, MD)  == 0 ||
				strcmp(file_extension, "json")  == 0 ||
				strcmp(file_extension, "yml")  == 0 ||
				strcmp(file_extension, "cmake")  == 0)
		{
			return 1;
		} else {
			return 0; 
		}
	}
	return 0;
}

long init_file_read(char *fileName)
{
	// define file stream
	FILE *stream;
	
	if ((stream = fopen(fileName, "rb")) != NULL)
	{
		fseek(stream, 0L, SEEK_END);
		long bufferSize = ftell(stream);
		fseek(stream, 0L, SEEK_SET);
		
		// +1 for null terminator '\0' 
		char *buffer = malloc(sizeof(char) * bufferSize + 1);
		
		// init buffer with 0s
		memset(buffer, 0, sizeof(char) * bufferSize);
		int fileSizeInBytes = fread(buffer, bufferSize, 1, stream);
		
		if (fileSizeInBytes == 0)
		{
			free(buffer);
			fclose(stream);
			return bufferSize;
		} else {
			if (ferror(stream))
			{ 
				printf("Error reading file\n");
				free(buffer);
			} else if (feof(stream)) { 
				printf("EOF found\n");
				free(buffer);
			}
		}
		free(buffer);
		fclose(stream);
		return bufferSize;
	} else {
		printf("Error opening %s\n", fileName);
		return -1;
	}
	free(fileName);
	fclose(stream);
}

char *get_just_file_name(char *fullFileName)
{
	size_t c = 1;
	size_t lastChar = strlen(fullFileName);
	char *justFileNameResult = (char*)malloc(c * sizeof(char));
	*justFileNameResult = '\0';
	
	for(size_t i = lastChar; i > 0; i--)
	{
		if(fullFileName[i] == '\\')
		{
			break;
		} else {
			c++;
			justFileNameResult = realloc(justFileNameResult, c * sizeof(char) + 1);
			if (justFileNameResult == NULL)
			{
				printf("Error during memory reallocation for string appending.\n");
				return "Error occurred in function: char *get_just_file_name(char *fullFileName)\n";
			}
			append(justFileNameResult, fullFileName[i]);
		}
	}
	strrev(justFileNameResult);
	return justFileNameResult;
}

void append(char *s, char c)
{
	size_t len = strlen(s);
	s[len] = c;
	s[len + 1] = '\0';
}

void get_file_size(char *fileName, 
                   long *fileSize, 
                   long *initFileSize, 
                   int   firstPass,
									 char **buildPath,
									 char *cdToBuildPath)
{
	int next_file = 0;
	long temp = *fileSize;
	*initFileSize = init_file_read(fileName);
	char *currFile = get_just_file_name(fileName);
	
	while (!next_file)
	{
		*fileSize = init_file_read(fileName);
		
		if (*initFileSize != temp)
		{
			if (*fileSize != 0)
			{
				if (firstPass)
				{
					printf("Initial size of file %s: %ld bytes\n", currFile, *fileSize);
				} else {
					printf("File changed -- %s size is now: %ld\n", currFile, *fileSize);
					printf("Running build script\n");
					printf("Pushing to build path\n\n");
					
					// Run build script once change is detected
					char *pushCommand = malloc(sizeof(char) * (strlen(cdToBuildPath) + 4));
					strcat(pushCommand, "PUSHD");
					strcat(pushCommand, cdToBuildPath);
					system(pushCommand);
					system(BUILD_BAT);
					system("POPD");
					
					// Free allocated memory for system command
					free(pushCommand);
				}
				*initFileSize = *fileSize;
				next_file = 1;
			}
			else {
				next_file = 1;
			}
		} else { 
			next_file = 1;
		}
	}
	free(currFile);
}

void show_found_files(char *files[])
{
	printf("\n\nThe following should be a list of all the files\n");
	printf("that will be watched in your prject:\n");
	int currentFile = 0;
	while (files[currentFile] != NULL)
	{
		printf(" -- %s --\n", files[currentFile]);
		++currentFile;
	}
}

void file_check(int  *current_file, 
                char *buffer[], 
                char *fileName, 
                char *dirToWatch)
{
	
	if (buffer[(*current_file)] == NULL)
	{
		buffer[(*current_file)] = malloc(strlen(dirToWatch) + strlen(fileName) + WIN_FILE_SEPARATOR_LEN + 1);
		strcpy(buffer[(*current_file)], dirToWatch);
		strcat(buffer[(*current_file)], WIN_FILE_SEPARATOR);
		strcat(buffer[(*current_file)], fileName);
	} else { 
		(*current_file)++;
		file_check(current_file, buffer, fileName, dirToWatch);
	}
}

void get_length_of_extension_name(char *fileName, 
                                  int *extensionLength, 
                                  int *charsToDot,
                                  size_t *fileNameLength)
{
	(*fileNameLength) = strlen(fileName);
	while (fileName[(*fileNameLength) - (*charsToDot)] != '.')
	{
		(*charsToDot)++;
		(*extensionLength)++;
	}
}

void print_tab_per_level(int level)
{
	for(int i = 0; i < level; i++)
	{
		printf("\t");
	}
}

void use_bat_or_shell(char **buildPath)
{
	if(USE_BAT || USE_SH)
	{
		printf("\nBuild script found.\n"); 
		strcat(*buildPath, BUILD_BAT);
		printf("Path to script is: %s\n\n", *buildPath);
	} else {
		printf("No build script found.\n");
		printf("Defaulting to watch.\n");
	}
}

void print_usage() {
	printf(USAGE_BAR);
	printf(USAGE_TITLE);
	printf(USAGE_BAR);
	
	printf("\nYou can pass a total of 3 arguments to Wacther.\n\n");
	printf("arg 1 -- true/false -- watch subdirectories -- by default, Watcher will watch all subdirectories.\n");
	printf("arg 2 -- exclude files -- give Watcher a file extension (e.g. txt, bin, png) and Watcher will ignore the file type.\n");
	printf("arg 3 -- exclude directory -- give Watcher a directory to exclude and Watcher will ignore it.\n");
}

int main(int argc, char *argv[])
{   
	/*
	 *
	 * There are most definitely some memory leaks going on here but for now, 
	 * we have fully functioning file watcher which (in theory) can scale up to many MANY files! 
	 *
	 */
	
	/*
	 * Get OS... this will matter a lot more later
	 */
#ifdef _WIN32
	printf("Windows OS detected.\n");
	
#elif _APPLE_
	printf("Mac OS detected.\n");
	
#elif _linux_
	printf("Linux OS detected.\n");
	
#elif _unix_
	prinf("Unix based OS detected.\n");
#endif
	
	/*
	 * main variables
	 */
	const char *OS;
	int levels = 0;
	int fileListPos = 0;  
	int totalFiles = 0;
	int firstPass = 1;
	long *fileSizes = 0;
	long *initFileSizes = 0;
	size_t lengthOfFoundFiles = 0;
	size_t cleanFileListSize = 0;
	char *buildPath = NULL;
	char *cdToBuildPath = NULL;
	char *workingDir = malloc(sizeof(char) * MAX_DIRS + 1);
	char *fullFileList[MAX_FILES] = { { 0 } };
	char **cleanFileList = NULL;
	
	
	/*
	 * Parse user specified arguments
	 */
	/*
	NOTE (Lake): DO NOT DELETE YET


	if (argc == 1 || 
			strcmp(argv[1], "h") == 0 || 
			strcmp(argv[1], "help") == 0 )
	{
			print_usage();
	} else {
			if(parse_args(argv[1], argv[2], argv[3]))
			{
					printf("\nStarting Wacther. Indexing your project. Use CTL+C to exit program\n");
			} else {
					printf("See usage.\n\n");
					print_usage();
			}
	}
	*/
	
	/*
	 * Index directory structure from the current working directory
	 */
	printf("Watcher is indexing your project. This can take some time depending on the size of your project.\n\n");
	
	_getcwd(workingDir, MAX_DIRS);
	if(check_current_dir(workingDir, 0, &levels, &buildPath, fullFileList, &totalFiles))
	{
		printf("\nIndex complete. Above should be the the structure of your project.");
	} else {
		printf("Indexing failed.\n\n");
		return -1;
	}
	
	/*
	 * Show files found by indexing process
	 */
	show_found_files(fullFileList);
	
	/*
	 * Use bat or shell build file
	 */
	if(strlen(buildPath) > 0) {
		cdToBuildPath = malloc(sizeof(char) * strlen(buildPath));
		strcpy(cdToBuildPath, buildPath);
	}
	use_bat_or_shell(&buildPath);
	
	
	/*
	 * Start watcher
	 */
	printf("Starting Wacther. Use CTL+C to exit program\n");
	
	lengthOfFoundFiles = sizeof(fullFileList) / sizeof(fullFileList[0]);
	
	while(fullFileList[cleanFileListSize] != NULL)
	{
		cleanFileListSize++;
	}
	
	cleanFileList = malloc(sizeof(char*) * cleanFileListSize + 1);
	
	
	for(size_t i = 0; i <= cleanFileListSize; i++)
	{
		if (fullFileList[i] != NULL)
		{
			size_t lengthOfFileName = strlen(fullFileList[i]);
			cleanFileList[i] = malloc(sizeof(char*) * lengthOfFileName + 1);
			strcpy(cleanFileList[i], fullFileList[i]);
			size_t lengthOfCleanFileName = strlen(fullFileList[i]);
		}
	}
	
	
	/*
	 * This looks really weird but for a good reason...
	 * if we were to call free(fullFileList[i]) here then the program would crash because
	 * the actual array is stack allocated, NOT heap allocated
	 */ 
	for (int i = 0; i <= lengthOfFoundFiles; i++)
	{
		fullFileList[i] = NULL;
	}
	
	
	
	/*
	 * Allocate memory for a list of file sizes for each of the files
	 */
	fileSizes = (long*)malloc(sizeof(long) * cleanFileListSize);
	initFileSizes = (long*)malloc(sizeof(long) * cleanFileListSize);
	memset(fileSizes, 0, cleanFileListSize);
	memset(initFileSizes, 0, cleanFileListSize);
	
	
	/*
	 * Iterate through the clean file list and watch the specified files
	 * This is like a pseudo 'while(true)' super hacky, but whatever
	 */
	for(size_t i = 0; i <= cleanFileListSize; i++)
	{
		if(cleanFileList[i] == NULL)
		{
			i = 0;
			firstPass = 0;
			continue;
		} else {
			if (firstPass) 
			{
				get_file_size(cleanFileList[i], &fileSizes[i], &initFileSizes[i], firstPass, &buildPath, cdToBuildPath);
			} else {
				firstPass = 0;
				get_file_size(cleanFileList[i], &fileSizes[i], &initFileSizes[i], firstPass, &buildPath, cdToBuildPath);
			}
		}
	}
	
	/*
	 * Free memory
	 */
	free(fileSizes);
	free(initFileSizes);
	free(workingDir);
	free(buildPath);
	
	/*
	 * Done.
	 */
	printf("Done.\n");
}
